import featuresection1dottedrows from "../images/feature-section1-dottedrows.png";
import paymentsuite from "../images/payment-suite.png";
import paymentlinkicon from "../images/payment-link-icon.svg";

const Features = () => {
return (
    <section class="relative mt-[190px] overflow-hidden">
      <img src={featuresection1dottedrows} alt="" loading="lazy" 
      width="233"
      height="167"
      class="absolute -top-[8rem] left-[10rem] inline-block -z-10"/>

      <img src={featuresection1dottedrows} alt="" loading="lazy"
      width="233"
      height="167"
      class="absolute top-[3rem] right-0 inline-block rotate-180"/>

      <div class="relative w-11/12 max-w-[1080px] mx-auto pt-4">
     
          <h2 class="font-mullish text-center text-2xl leading-[1.2] font-extrabold hidden md:block">Accept Payments with Razorpay Payment Suite</h2>
          
          <h2 class="font-mullish text-center text-5xl leading-[1.2] font-extrabold  md:hidden">Explore Razorpay Payment Suite</h2>


          <div class="w-6 h-1 bg-greenLight mx-auto mt-4 mb-6"></div>

     
          <div class="w-full min-h-[520px] bg-white flex rounded-md relative p-10 py-12 border">
  
              <div class="flex flex-col justify-between  w-full">
                  <h3 class="font-mullish text-[28px] leading-10 font-bold max-w-[500px]">
                      Supercharge your business with the all‑powerful
                      <span class="text-lightBlue">Payment Gateway</span>
                  </h3>
                  <ul class="space-y-2">
                      <li class="font-mullish flex items-center space-x-2">
                          
                          <i data-feather="check" class="text-greenLight"></i>
                          <span>100+ Payment Methods</span>
                      </li>
                      <li class="font-mullish flex items-center space-x-2">
                          <i data-feather="check" class="text-greenLight"></i>
                          <span> Industry Leading Success Rate </span>
                      </li>
                      <li class="font-mullish flex items-center space-x-2">
                          <i data-feather="check" class="text-greenLight"></i>
                          <span> Superior Checkout Experience </span>
                      </li>
                      <li class="font-mullish flex items-center space-x-2">
                          <i data-feather="check" class="text-greenLight"></i>
                          <span> Easy to Integrate </span>
                      </li>
                      <li class="font-mullish flex items-center space-x-2">
                          <i data-feather="check" class="text-greenLight"></i>
                          <span> Instant Settlements from day 1 </span>
                      </li>
                      <li class="font-mullish flex items-center space-x-2">
                          <i data-feather="check" class="text-greenLight"></i>
                          <span> In-depth Reporting and Insights </span>
                      </li>
                  </ul>

           
              <div class="flex flex-col-reverse md:flex-row items-center space-x-4 ">
                  <button
                  class="bg-lightBlue w-full md:w-fit text-white py-[14px] px-[18px] rounded-md
                  font-mullish font-bold hover:bg-lightBlue500 translate-all duration-200 "
                  >Sign Up Now</button>
     
                  <div class="flex self-start md:items-center cursor-pointer group">
                      <a href="#" 
                      class="font-mullish font-bold text-lightBlue500 
                      group-hover:text-grayBlue transition-all duration-200"
                      >Know More</a>
                      <i
                      data-feather="chevron-right"
                      class="w-5 h-5 text-lightBlue500 
                      group-hover:text-grayBlue transition-all duration-200"></i>
                  </div>
              </div>

              </div>
              <img src={paymentsuite} alt=""
              class="max-w-[600px] absolute right-0 bottom-0 hidden md:max-w-[400px] lg:max-w-[600px] md:block lg:block"/>
          </div>

          <div class="w-full grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-4 mt-10">
 
              <div class="w-full min-h-[15rem] relative cursor-pointer">
                  <img src={paymentlinkicon} alt=""
                  class="bg-lightBlue absolute right-3 top-3 w-12 h-12 rounded-full z-[8]
                  transition-all duration-200"/>
                  
                  
                 
  
                  
                  <div class="z-[100] absolute w-full h-full flex flex-col justify-between pl-5 py-6 pr-8">
                      
                      <div>
                          <h3 class="font-mullish font-bold text-deepBlueHead leading-[1.2] text-[1.375rem]">Payment Links</h3>
                          <p class="font-mullish text-grayText mt-6">
                              Share payment link via an email, SMS, messenger, chatbot etc. and get paid immediately</p>
                      </div>
  
                      
                      <div class="flex items-center cursor-pointer group">
                          <a href="#" 
                          class="font-mullish font-bold text-lightBlue500 
                          group-hover:text-grayBlue transition-all duration-200"
                          >Know More</a>
                          <i
                          data-feather="chevron-right"
                          class="w-5 h-5 text-lightBlue500 
                          group-hover:text-grayBlue transition-all duration-200"></i>
                      </div>
  
                  </div>
  
              </div>
    
              <div class="w-full min-h-[15rem] relative cursor-pointer">
                  <img src={paymentlinkicon} alt=""
                  class="bg-lightBlue absolute right-3 top-3 w-12 h-12 rounded-full z-[8]
                  transition-all duration-200"/>
                  
                  
                 
  
                  
                  <div class="z-[100] absolute w-full h-full flex flex-col justify-between pl-5 py-6 pr-8">
                      
                      <div>
                          <h3 class="font-mullish font-bold text-deepBlueHead leading-[1.2] text-[1.375rem]">Payment Links</h3>
                          <p class="font-mullish text-grayText mt-6">
                              Share payment link via an email, SMS, messenger, chatbot etc. and get paid immediately</p>
                      </div>
  
                      
                      <div class="flex items-center cursor-pointer group">
                          <a href="#" 
                          class="font-mullish font-bold text-lightBlue500 
                          group-hover:text-grayBlue transition-all duration-200"
                          >Know More</a>
                          <i
                          data-feather="chevron-right"
                          class="w-5 h-5 text-lightBlue500 
                          group-hover:text-grayBlue transition-all duration-200"></i>
                      </div>
  
                  </div>
  
              </div>
   
              <div class="w-full min-h-[15rem] relative cursor-pointer">
                  <img src={paymentlinkicon} alt=""
                  class="bg-lightBlue absolute right-3 top-3 w-12 h-12 rounded-full z-[8]
                  transition-all duration-200"/>
                  
                  
                 
  
                  
                  <div class="z-[100] absolute w-full h-full flex flex-col justify-between pl-5 py-6 pr-8">
                      
                      <div>
                          <h3 class="font-mullish font-bold text-deepBlueHead leading-[1.2] text-[1.375rem]">Payment Links</h3>
                          <p class="font-mullish text-grayText mt-6">
                              Share payment link via an email, SMS, messenger, chatbot etc. and get paid immediately</p>
                      </div>
  
                      
                      <div class="flex items-center cursor-pointer group">
                          <a href="#" 
                          class="font-mullish font-bold text-lightBlue500 
                          group-hover:text-grayBlue transition-all duration-200"
                          >Know More</a>
                          <i
                          data-feather="chevron-right"
                          class="w-5 h-5 text-lightBlue500 
                          group-hover:text-grayBlue transition-all duration-200"></i>
                      </div>
  
                  </div>
  
              </div>
   
              <div class="w-full min-h-[15rem] relative cursor-pointer">
                  <img src={paymentlinkicon} alt=""
                  class="bg-lightBlue absolute right-3 top-3 w-12 h-12 rounded-full z-[8]
                  transition-all duration-200"/>
                  
                  
                 
  
                  
                  <div class="z-[100] absolute w-full h-full flex flex-col justify-between pl-5 py-6 pr-8">
                      
                      <div>
                          <h3 class="font-mullish font-bold text-deepBlueHead leading-[1.2] text-[1.375rem]">Payment Links</h3>
                          <p class="font-mullish text-grayText mt-6">
                              Share payment link via an email, SMS, messenger, chatbot etc. and get paid immediately</p>
                      </div>
  
                      
                      <div class="flex items-center cursor-pointer group">
                          <a href="#" 
                          class="font-mullish font-bold text-lightBlue500 
                          group-hover:text-grayBlue transition-all duration-200"
                          >Know More</a>
                          <i
                          data-feather="chevron-right"
                          class="w-5 h-5 text-lightBlue500 
                          group-hover:text-grayBlue transition-all duration-200"></i>
                      </div>
  
                  </div>
  
              </div>
     
              <div class="w-full min-h-[15rem] relative cursor-pointer">
                  <img src={paymentlinkicon} alt=""
                  class="bg-lightBlue absolute right-3 top-3 w-12 h-12 rounded-full z-[8]
                  transition-all duration-200"/>
                  
                  
                 
  
                  
                  <div class="z-[100] absolute w-full h-full flex flex-col justify-between pl-5 py-6 pr-8">
                      
                      <div>
                          <h3 class="font-mullish font-bold text-deepBlueHead leading-[1.2] text-[1.375rem]">Payment Links</h3>
                          <p class="font-mullish text-grayText mt-6">
                              Share payment link via an email, SMS, messenger, chatbot etc. and get paid immediately</p>
                      </div>
  
                      
                      <div class="flex items-center cursor-pointer group">
                          <a href="#" 
                          class="font-mullish font-bold text-lightBlue500 
                          group-hover:text-grayBlue transition-all duration-200"
                          >Know More</a>
                          <i
                          data-feather="chevron-right"
                          class="w-5 h-5 text-lightBlue500 
                          group-hover:text-grayBlue transition-all duration-200"></i>
                      </div>
  
                  </div>
  
              </div>
             
              <div class="w-full min-h-[15rem] relative cursor-pointer">
                  <img src={paymentlinkicon} alt=""
                  class="bg-lightBlue absolute right-3 top-3 w-12 h-12 rounded-full z-[8]
                  transition-all duration-200"/>
                  
                  
                 
  
                  
                  <div class="z-[100] absolute w-full h-full flex flex-col justify-between pl-5 py-6 pr-8">
                      
                      <div>
                          <h3 class="font-mullish font-bold text-deepBlueHead leading-[1.2] text-[1.375rem]">Payment Links</h3>
                          <p class="font-mullish text-grayText mt-6">
                              Share payment link via an email, SMS, messenger, chatbot etc. and get paid immediately</p>
                      </div>
  
                      
                      <div class="flex items-center cursor-pointer group">
                          <a href="#" 
                          class="font-mullish font-bold text-lightBlue500 
                          group-hover:text-grayBlue transition-all duration-200"
                          >Know More</a>
                          <i
                          data-feather="chevron-right"
                          class="w-5 h-5 text-lightBlue500 
                          group-hover:text-grayBlue transition-all duration-200"></i>
                      </div>
  
                  </div>
  
              </div>
          </div>

      </div>



  </section>
  )
}

export default Features;