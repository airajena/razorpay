import './App.css'
import Header from './Components/Header'
import './index.css'
import Hero from './Components/Hero'
import Features from './Components/Features'


function App() {

  return (
    <>
      <Header/>
      <Hero/>
      <Features/>
 
    </>
  )
}

export default App
